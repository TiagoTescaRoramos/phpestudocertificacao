<?php
/* 
 * PHP Data Objects - PDO
 * É uma interface para acessar diversos bancos de dados.
 *
 * Para conectar a base, é preciso criar uma instância da classe PDO
 * new PDO('<driver>:host;Base','user','password')
 */
try{
    $con = new PDO('mysql:localhost;testeTiago','root','');
} catch (PDOException $ex) {
    echo "{$ex->getCode()}:{$ex->getMessage()} - {$ex->getLine()}" . PHP_EOL . PHP_EOL;
}

/*
 * Para verificarmos quais bases o server aceita, podemos utilizar o método PDO::getAvailableDrivers()
 */
try{
    print_r(PDO::getAvailableDrivers());
} catch(PDOException | Error $ex){
    echo "{$ex->getCode()}:{$ex->getMessage()} - {$ex->getLine()}" . PHP_EOL . PHP_EOL;
}

/*
 * Verifica se a transação está ativa ou não para o drive selecionado. Caso o driver não suporte transação, ao executar o método, ocorre fatal erro.
 */
try{
    var_dump(PDO::inTransaction());
} catch (PDOException | Error $ex) {
    echo "{$ex->getCode()}:{$ex->getMessage()} - {$ex->getLine()}" . PHP_EOL . PHP_EOL;
}