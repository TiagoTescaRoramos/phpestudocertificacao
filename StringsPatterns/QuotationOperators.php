<?php

/*
 * String Operators - Operadores de Strings
 * Temos dois operadores, aspas simples (') ou aspas duplas(")
 * 
 * Aspas Simples - Printa uma string, o PHP não faz a verificação, se no print tem alguma variavel ou objeto.
 *
 * Aspas Duplas - O PHP faz a verificação se na String existe variável ou Objeto na String.
 * Atenção - O PHP verifica somente variável e objeto (Incluí propriedades e métodos), mas 
 *           funções nativas e criadas pelo programador não.
*/

function func($name){
    return "Hello $name";
}
echo 'Abaixo ocorre erro, pois a função está em aspas duplas.' . PHP_EOL;
echo "{func('Tiago')}" . PHP_EOL;
echo PHP_EOL;

echo 'Agora funcionou, função está fora de string.' . PHP_EOL;
echo func('Tiago') . PHP_EOL;
echo PHP_EOL;