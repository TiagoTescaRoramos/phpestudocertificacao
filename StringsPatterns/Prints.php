<?php

/*
 * format printf
 * % - Um caractere porcento. Não é requerido neenhum argumento.
 * b - O argumento é tratado com um inteiro, e mostrado como um binário.
 * c - O argumento é tratado como um inteiro, e mostrado como o caractere ASCII correspondente.
 * d - O argumento é tratado como um inteiro, e mostrado como um número decimal com sinal.
 * e - o argumento é tratado como notação científica (e.g. 1.2e+2). O especificador de precisão indica o número de dígitos depois do ponto decimal desde o PHP 5.2.1. Em versões anteriores, ele pegava o número de dígitos significantes (ou menos).
 * u - O argumento é tratado com um inteiro, e mostrado como um número decimal sem sinal.
 * f - O argumento é tratado como um float, e mostrado como um número de ponto flutuante (do locale).
 * F - o argumento é tratado como um float, e mostrado como um número de ponto flutuante (não usado o locale). Disponível desde o PHP 4.3.10 e PHP 5.0.3.
 * o - O argumento é tratado com um inteiro, e mostrado como un número octal.
 * s - O argumento é tratado e mostrado como uma string.
 * x - O argumento é tratado como um inteiro, e mostrado como um número hexadecimal (com as letras minúsculas).
 * X - O argumento é tratado como um inteiro, e mostrado como um número hexadecimal (com as letras maiúsculas).
 * $ - Indicar quando você quer indicar a ordem recebida. 
 *      Exemplo: 
 *          $format = 'The %2$s contains %1$d monkeys';
 *          printf($format,$num,$location);
 */

//Esse caracter '2$', é a order recebida na função. Esse '2$ representa a segunda colocação
$format = 'The %2$s contains %1$d monkeys';
$num = 12;
$location = 'Brazil';
printf($format,$num,$location);
echo PHP_EOL;

//Dessa forma, respeita a order informada na função. A variavel $num será primeiro e assim sucessivamente.
$format = 'The %s contains %d monkeys';
printf($format,$num,$location); 
echo PHP_EOL;