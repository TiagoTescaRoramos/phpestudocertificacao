<?php

/*
 * Tipos de variáveis:
 * 
 *   Quatro tipos escalares:
 *      - boolean
 *      - integer
 *      - float (número de ponto flutuante, ou também double)
 *      - string
 *
 *  Três tipos compostos:
 *      - array
 *      - object
 *      - callable
 *
 *   E finalmente dois tipos especiais:
 *       - resource
 *       - NULL
*/