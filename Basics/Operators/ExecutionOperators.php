<?php

/* 
 * Execution Operators - Operadores de Execução
 * Quando é dado echo com backticks - acento grave (``). Ele executa um comando via Shell
 * 
 * Nota:
 * O operador de execução fica desabilitado quando safe mode está ativo ou shell_exec() está desabilitado.
 */
$comando = `ls -lah`;
echo "<pre>$comando</pre>"; // Mesmo colocando a variavel em aspas duplas, o comando é executado

