<?php

/**
 * Operator Equal - Operador de Igual
 * ==
 */
$a = 'T';
$b = 'T';
var_dump($a == $b); //True
echo PHP_EOL;

/*
 * Operator Identical - Operador de Indêntico
 * ===
 */

echo 'False. Pois está verificando além do valor, a tipagem também.' . PHP_EOL;
$a = true;
$b = 'true';
var_dump($a === $b);//False
echo PHP_EOL;

/*
 * Operators not equal - Operadores não igual
 * != or <> 
 */
echo 'False. Tipos diferentes, mas o conteúdo igual. O PHP faz a conversão' . PHP_EOL;
var_dump($a != $b);
$a = 'T';
$b = 'K';
echo 'True. O conteúdo é diferente' . PHP_EOL;
var_dump($a <> $b);
echo PHP_EOL;

/*
 * Operator not identical - Operador não indêntico
 * !==
 */
$a = false;
$b = 'false';
echo 'True. Além de verificar o conteúdo, é verificado a tipagem das variaveis.' . PHP_EOL;
var_dump($a !== $b);
echo PHP_EOL;

/*
 * Operator Less than - Operador Menor que
 * <
*/

$a = 2;
$b = 6;
echo 'True. 2 é menor que 6.' . PHP_EOL;
var_dump($a < $b);
echo PHP_EOL;

/*
 * Operator Greater than - Operador maior que
 * >
*/
echo 'True. 6 é maior que 2.' . PHP_EOL;
var_dump($b > $a);
echo PHP_EOL;

/*
 * Operator Less than or equal to - Menor ou igual
 * <=
*/
$a = 6;
$b = 6;
$c = 2;
echo 'True. $a = 6 é igual $b = 6 e $c = 2 é menor que $a = 6' . PHP_EOL;
var_dump($a <= $b && $c <= $a);
echo PHP_EOL;

/*
 * Operator Greater than or equal to - Operador maior ou igual
*/
echo 'True. $a = 6 é igual $b = 6 e $a = 6 é maior que $c = 2' . PHP_EOL;
var_dump($a >= $b && $a >= $c);
echo PHP_EOL;

/*
 * Operator Spaceship
 * <=>
 * PHP 7
*/
$a = 1;
$b = 2;
echo '-1. $a = 1 é menor que $b = 2' . PHP_EOL;
var_dump($a <=> $b);
echo PHP_EOL;

echo '1. $b = 2 é maior que $a = 1' . PHP_EOL;
var_dump($b <=> $a);
echo PHP_EOL;

$a = 1;
$b = 1;
echo '1. $a = 1 é igual a $b = 1' . PHP_EOL;
var_dump($a <=> $b);
echo PHP_EOL;

/*
 * Operator Null coalescing
 * $a ?? $b ?? $c
 * PHP7
 * Verifica da esqueda para direita. Se a primeira variavel for null, retorna a próxima e assim por diante
*/
$a = 'T';
$b = 'K';
$c = 'E';
echo 'T. Não existe nenhum valor nulo' . PHP_EOL;
var_dump($a ?? $b ?? $c);
echo PHP_EOL;

$a = NULL;
echo 'K. Pois a primeira variavel $a = null' . PHP_EOL;
var_dump($a ?? $b ?? $c);
echo PHP_EOL;

$b = NULL;
echo 'E. Pois a variavel $a e $b são nulos então retorna $c' . PHP_EOL;
var_dump($a ?? $b ?? $c);
echo PHP_EOL;

$c = NULL;
echo 'NULL. Pois todas as variaveis são nulos, por isso retorna nulo.'. PHP_EOL;
var_dump($a ?? $b ?? $c);
echo PHP_EOL;