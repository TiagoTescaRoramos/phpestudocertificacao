<?php

/*
 * Assignment Operator - Operador de Atribuição
 * The basic assignment operators is '='
 */
$a = 'Hello Tiago';
echo $a . PHP_EOL;

echo 'O resultado é 5, pois o PHP faz a atribuição primeiro, depois efetua a soma.' . PHP_EOL;
$a = ($b = 3) + 2;
echo $a . PHP_EOL;
echo PHP_EOL;

echo 'O resultado é 13. O PHP faz as primeiras atribuições. No terceiro parentes, ($b *= 2),' . PHP_EOL;
echo 'a variável $d já está valendo 3, então faz (3 * 2)' . PHP_EOL;
echo ($c = 2) * 2 + ($d = 3) + ($d *= 2) . PHP_EOL;
echo PHP_EOL;

echo 'Operador de Atribuição. O = zera a variavel, não respeitando o valor antigo dela.' . PHP_EOL;
$a = 2;
echo $a . PHP_EOL;
echo PHP_EOL;

echo 'Soma, (2 + 5). Pois a atribuição agora está respeitando o valor antigo. +=' . PHP_EOL;
$a += 5;
echo $a . PHP_EOL;
echo PHP_EOL;

echo 'Multiplicação, (7 * 2). Pois a atribuição agora está respeitando o valor antigo. *=' . PHP_EOL;
$a *= 2;
echo $a . PHP_EOL;
echo PHP_EOL;

echo 'Operadores de Referência' . PHP_EOL;
$nome = 'Tiago';
echo $nome . PHP_EOL;
echo PHP_EOL;

echo 'Atribuindo "(tabulação)Tescaro", respeitando o valor antigo. (.=)' . PHP_EOL;
$nome .= "\tTescaro";
echo $nome . PHP_EOL;
echo PHP_EOL;

echo 'Referenciando a variável $nome. O operador (&) utiliza o mesmo local de memória.' . PHP_EOL;
echo 'Com isso, se eu alterar a variável $nomeCompleto, a variável $nome também é alterada.' . PHP_EOL;
$nomeCompleto = &$nome;
echo $nomeCompleto . PHP_EOL;
echo PHP_EOL;

echo 'Agora estou retirando a tabulação da variável $nomeCompleto (str_replace("\t"," ",$nomeCompleto))' . PHP_EOL;
echo 'Após isso, estou dando um echo nas variáveis $nomeCompleto e $nome. Ambas está sem a tabulação.' . PHP_EOL;
echo 'Pois alterei o mesmo local de memória.' . PHP_EOL;
$nomeCompleto = str_replace("\t",' ',$nomeCompleto);
echo $nomeCompleto . PHP_EOL;
echo $nome . PHP_EOL;