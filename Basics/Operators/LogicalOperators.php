<?php

/* 
 * Logical Operators - Operadores Lógicos
 * Como o proprio nome diz, são operadores de lógica.
 * 
 * Temos os Operadores:
 * 
 * $a and $b	E	    Verdadeiro (TRUE) se tanto $a quanto $b são verdadeiros.
 * $a or $b	    OU	    Verdadeiro se $a ou $b são verdadeiros.
 * $a xor $b	XOR	    Verdadeiro se $a ou $b são verdadeiros, mas não ambos.
 * !$a          NÃO	    Verdadeiro se $a não é verdadeiro.
 * $a && $b	    E	    Verdadeiro se tanto $a quanto $b são verdadeiros.
 * $a || $b	    OU	    Verdadeiro se $a ou $b são verdadeiros.
 */

echo 'Podemos utilizar os operadores  (and, &&)' . PHP_EOL;
echo '(true and false)' . ((true and false) ? 'TRUE' : 'FALSE') . PHP_EOL;
echo 'Retorna FALSO por que o if ternário verifica se os dois valores são verdadeiros.' . PHP_EOL;
echo PHP_EOL;

echo 'Podemos utilizar os operadores (or,||)' . PHP_EOL;
echo '(true or false)' . ((true or false) ? 'TRUE' : 'FALSE') . PHP_EOL;
echo 'Retorna TRUE por que o if ternário verifica se uma das condições forem verdadeiras.' . PHP_EOL;
echo PHP_EOL;

echo 'Podemos utilizar os operadores (xor)' . PHP_EOL;
echo '(true xor false)' . ((true xor false) ? 'TRUE' : 'FALSE') . PHP_EOL;
echo PHP_EOL;
echo 'Retorna TRUE por que o if ternário verifica se uma das condições forem verdadeiras.' . PHP_EOL;
echo '(true xor true)' . ((true xor true) ? 'TRUE' : 'FALSE') . PHP_EOL;
echo 'Mas se ambos forem verdadeiras ou ambas forem falsas. Retorna Falso.' . PHP_EOL;
echo PHP_EOL;

echo 'Podemos utilizar os operadores (!)' . PHP_EOL;
echo '(!true)' . (!true ? 'TRUE' : 'FALSE') . PHP_EOL;
echo 'Retorna FALSO por que no if ternário, estou negando o TRUE.' . PHP_EOL;
echo PHP_EOL;
echo '(!false)' . (!false ? 'TRUE' : 'FALSE') . PHP_EOL;
echo 'Retorna TRUE por que no if ternário, estou negando o FALSE.' . PHP_EOL;
echo PHP_EOL;

