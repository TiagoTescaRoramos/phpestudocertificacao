<?php

/* 
 * Operadores bit-a-bit permitem a avaliação e modificação de bits específicos em um tipo inteiro.
 *  Exemplo	Nome                        Resultado
 *  $a & $b	E (AND)                     Os bits que estão ativos tanto em $a quanto em $b são ativados.
 *  $a | $b	OU (OR inclusivo)           Os bits que estão ativos em $a ou em $b são ativados.
 *  $a ^ $b	XOR (OR exclusivo)          Os bits que estão ativos em $a ou em $b, mas não em ambos, são ativados.
 *  ~ $a	NÃO (NOT)                   Os bits que estão ativos em $a não são ativados, e vice-versa.
 *  $a << $b	Deslocamento à esquerda     Desloca os bits de $a $b passos para a esquerda (cada passo significa "multiplica por dois")
 *  $a >> $b	Deslocamento à direita      Desloca os bits de $a $b passos para a direita (cada passo significa "divide por dois")
 */


$format = '(%1$2d = %1$04b) = (%2$2d = %2$04b)' . ' %3$s (%4$2d = %4$04b)' . PHP_EOL;

$a = 1;
$b = 2;
$c = $a & $b;
printf($format,$a,$b,$c,PHP_EOL);