<?php

/* 
 * Incrementing or Decrementing operators - Operadores de Incremento ou Decremento
 * No PHP temos como operadores de pré e pós Incremento ou Decremento
 */

$a = 1;
echo 'Incremento' . PHP_EOL;
echo ++$a . ' - $a = 1. O echo resulta em 2, pois é incremento pré. ++$a.' . PHP_EOL;
echo $a++ . ' - $a = 2. O echo resulta em 2 e não muda no echo, mas o $a é 3 agora, pois é um incremento pós. $a++' . PHP_EOL;
echo PHP_EOL;

echo 'Decremento' . PHP_EOL;
echo --$a . ' - $a = 3. O echo resulta em 2, pois é um decremento pré. --$a' . PHP_EOL;
echo $a-- . ' - $a = 2. O echo resulta em 2 e não muda no echo, mas o $a é 1 agora, pois é um decremento pós. $a--' . PHP_EOL;