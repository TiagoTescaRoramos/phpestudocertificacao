<?php

/*
 * No PHP, quando você coloca o arroba '@' na frente da função. O PHP ignora a mensagem de erro para não ser mostrada
 */

//Para mostrar erros, caso no servidor esteja desabilitado.
error_reporting(E_ALL);
echo $a;// Ocorre o erro

echo @$a;// Não ocorre erro devido ao '@';