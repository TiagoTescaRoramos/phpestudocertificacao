<?php

/*
 * Identity
 * Conversão de $a para int ou float conforme apropriado.
 * Lembrando que ocorre um warning nessa operação.
 */
$a = '12 Tiago';
echo +$a . PHP_EOL;

/*
 * Negation
 * Negativa o valor.
 */

$b = 20;
echo -$b . PHP_EOL;

/*
 * Addition
 * Sum of $a and $b
 */
$a = 10;
$b = 15;
echo $a + $b . PHP_EOL;

// Sum with float
$a = 10.5;
$b = 15;
echo $a + $b . PHP_EOL;


/*
 * Subtraction
 * Subtraction of $a and $b
 */
$a = 15;
$b = 30;
echo $b - $a . PHP_EOL;

//Subtraction with float
$a = 15.4;
$b = 30.9;
echo $b - $a . PHP_EOL;

/*
 * Multiplication
 * Multiplication of $a and $b
 */
$a = 5;
$b = 2;
echo $a * $b . PHP_EOL;

//Multiplication with float
$a = 5.2;
$b = 2.8;
echo $a * $b . PHP_EOL;

/*
 * Division
 * Division of $a and $b
 */
$a = 4;
$b = 2;
echo $a / $b . ' Division'.PHP_EOL;

//Division with float
$a = 4.2;
$b = 2.2;
echo $a / $b . PHP_EOL;

/*
 * Modulo
 * Remainder of $a and $b (Restante de $a dividido por $b)
 */
$a = 3;
$b = 2;
echo $a % $b . PHP_EOL;

/*
 * Exponentiation
 * Result of raising $a to the $b'th power. Introduced in PHP 5.6.
 * Resultado de $a elevado a $b. Introduzido no PHP 5.6
 */
$a = 2;
$b = 3;
echo $a ** $b . PHP_EOL;