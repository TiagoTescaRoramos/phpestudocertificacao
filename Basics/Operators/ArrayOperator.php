<?php

/*
 * $a + $b 	União 	            União de $a e $b.
 * $a == $b 	Igualdade 	    TRUE se $a e $b tem os mesmos pares de chave/valor.
 * $a === $b 	Identidade 	    TRUE se $a e $b tem os mesmos pares de chave/valor na mesma ordem e do mesmo tipo.
 * $a != $b 	Desigualdade 	    TRUE se $a não é igual a $b.
 * $a <> $b 	Desigualdade 	    TRUE se $a não é igual a $b.
 * $a !== $b 	Não identidade 	    TRUE se $a não é identico a $b.
*/

/*
 * Operador União
 * Ele mescla dois arrays.
 * Caso os dois arrays, tenha chaves iguais, ele subscreve, permanecendo o primeiro (Da esquerda pra direita)
*/
$a = [
    'Marido' => 'Tiago',
    'Esposa' => 'Karen'
];
$b = [
    'Gato1' => 'Mimo',
    'Gato2' => 'Stuart'
];
print_r($a+$b);

/*
 * O exemplo abaixo, vai imprimir o array $a = ['Tiago','Karen']. Vai subescrever o array $b = ['Mimo','Stuart']
 */
$a = [
    'Tiago',
    'Karen'
];
$b = [
    'Mimo',
    'Stuart'
];
print_r($a+$b);